/*V. 1.0 
 * 
 * Pour la compilation nous avons utilisé les paramètres ci-après: 
 *  - UPLOAD SPEED : 115200
 *  - CPU FREQUENCY: 160MHz
 *  - FLASH SIZE   : 4M
 * 
 * CETTE VERSION-CI INCLUE LES AMéLIORATIONS EN PARLANT DE L'OPTIMISATION ET TRANSMITION DES DONNéES GET
 * LE PROJET EST DISPONIBLE SUR: 
 *  https://github.com/Jesman12/cible-escrime
 * DéVELOPPEUR: JESUS MANUEL CUERVO ITURBIDE/ANDRE LARA AVILA 
 * DERNIèRE MIS à JOUR: 29/10/2019
 */
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include "battery.h"

WiFiServer server(80);
HTTPClient http;
const char* ssid     = "raspi-webgui";
const char* password = "ChangeMe";

const char* host = "10.3.141.1"; //IL FAUDRA CHANGER L'IP CI-CÔTé LORS DE L'UTILISATION D'UN NOUVEAU SERVEUR (BOÎTE SERVEUR; PEUT-ÊTRE LA RPI OU QUELQUE SOIT LE SERVEUR UTILISé)

const int httpPort = 80;

long previousMillis = 0;
long intervalOn = 2000;
long intervalOff = 3000;

#define BUZZER 3 // (3)(D6)
#define LED1 12 //LEDs Petite Cible     (12)(D0)
#define LED2 14 //LEDs Moyenne Cible    (14)(D1)
#define LED3 13 //LEDs Grande Cible     (13)(D2)
#define BTN1 16 //Bouton Moyenne Cible   (16)(D3)
#define BTN2 5  //Bouton Petite Cible   (5)(D7)
#define BTN3 4  //Bouton Grande Cible   (4)(D5)
#define USB 15 //lecture de la pin USB (toujours positif meme sans etre déclaré)


byte state = 0;
byte prevState;

byte state2 = 0;
byte prevState2;

byte state3 = 0;
byte prevState3;

volatile bool bt1Press = false;
volatile bool bt2Press = false;
volatile bool bt3Press = false;

bool datos = false;
bool jeu = false;

String Jeu_Select = "0";
String id = "9"; //IL FAUDRA MODIFIER LA VALEUR LORSQU'ON VEUT CHANGER LE CIBLE. 
String IP;
String IPbis;
String url = "/Stage-FFE/form.php";
float min_value = 0.675;    //définition des valeurs de tension min et max de la batterie en sortie du pont
float max_value = 1;


void btn_1()
attachInterrupt(digitalPinToInterrupt(16), ISR, RISING);
 if(digitalRead(12) == HIGH){
      url = "/Stage-FFE/form.php?ip="+IPbis+"&btn=1";
      digitalWrite(LED1, LOW);
 }else{
      url = "/Stage-FFE/form.php?ip="+IPbis+"&btn=4";
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      digitalWrite(LED3, LOW);
      }
void btn_2()
attachInterrupt(digitalPinToInterrupt(5), ISR, RISING);
    if(digitalRead(14) == HIGH){
      url = "/Stage-FFE/form.php?ip="+IPbis+"&btn=2";
      digitalWrite(LED2, LOW);
    }else{
      url = "/Stage-FFE/form.php?ip="+IPbis+"&btn=4";
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      digitalWrite(LED3, LOW);
    }
void btn_3()
attachInterrupt(digitalPinToInterrupt(4), ISR, RISING);
  if(digitalRead(13) == HIGH){
      url = "/Stage-FFE/form.php?ip="+IPbis+"&btn=3";
      digitalWrite(LED3, LOW);
    }else{
      url = "/Stage-FFE/form.php?ip="+IPbis+"&btn=4";
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      digitalWrite(LED3, LOW);
    }
void setup()

{
  Serial.begin(9600);
  
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  pinMode(BTN1, INPUT);
  pinMode(BTN2, INPUT);
  pinMode(BTN3, INPUT);
  pinMode(USB,INPUT);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED1, HIGH);
    digitalWrite(LED2, HIGH);
    digitalWrite(LED3, HIGH);
    delay(500);
    digitalWrite(LED1, LOW);
    digitalWrite(LED2, LOW);
    digitalWrite(LED3, LOW);
    delay(500);
  }
  IP = WiFi.localIP().toString();
  IPbis = IP.substring(9); //9 digitosip
  //Serial.println(IPbis);
  digitalWrite(BUZZER, HIGH);
  delay(500);
  digitalWrite(BUZZER, LOW);  
  delay(500);
  digitalWrite(BUZZER, HIGH);
  delay(500);
  digitalWrite(BUZZER, LOW); 
  server.begin();
}

void loop(){
  state = digitalRead(BTN1);
  state2 = digitalRead(BTN2);
  state3 = digitalRead(BTN3);

  digitalWrite(BUZZER, LOW);
  unsigned long currentMillis = millis();

  WiFiClient client;
  if (!client.connect(host, httpPort)) {
    return;
  }
  while(!jeu){
    client = server.available();
    if (!client)
    {
      return;
    }
    client.println("HTTP/1.1 200 OK");
    client.println("Access-Control-Allow-Origin: *");
    client.println("Content-Type: text/html");
    client.println(""); 
    client.println("<!DOCTYPE HTML>");
    client.println("<html>");
    client.println("<br><br>");
    client.println("<a href=\"/?LED=100\"\"><button>PTS <b>5</b></button></a>");
    client.println("<a href=\"/?LED=010\"\"><button>PTS <b>2</b></button></a>");
    client.println("<a href=\"/?LED=001\"\"><button>PTS <b>1</b></button></a><br />");
    client.println("<a href=\"/?Start\"\"><button><b>JEU!</b></button></a><br />");
    //client.println("<a href=\"/?Niveau\"\"><button><b>Niveau</b></button></a><br />");
    client.println("id: "+id);
    client.println("IP: "+IP); 
    client.println("</html>");
    String line = client.readStringUntil('\r');
    client.flush();
    byte pos = line.indexOf("/?LED=");
    if(pos){
      String binario = line.substring(pos+6);
      String binario2 = binario.substring(0,3);
      String bL1 = binario2.substring(0,1);
      String bL2 = binario2.substring(1,2);
      String bL3 = binario2.substring(2,3);
      if(bL1 == "1"){
        digitalWrite(LED1, HIGH);
      }else{
        digitalWrite(LED1, LOW);
      }if(bL2 == "1"){
        digitalWrite(LED2, HIGH);
      }else{
        digitalWrite(LED2, LOW);
      }if(bL3 == "1"){
        digitalWrite(LED3, HIGH);
      }else{
        digitalWrite(LED3, LOW);
      }
      if(line.indexOf("&jeu=2") != -1){
        Jeu_Select = "2";
        previousMillis = currentMillis;
      }
      jeu = true;
    }
    if(line.indexOf("/?Nv_ID=") != -1){
      byte a = line.indexOf("/?Nv_ID=");
      byte c = line.indexOf(" HTTP/1.1");
      String b = line.substring(a+8,c);
      id=b;
      //Serial.println(b); 
    }
    if(line.indexOf("/?Start") != -1){
      for(int i = 0; i <= 3; i++){
        digitalWrite(LED1, HIGH);
        digitalWrite(LED2, HIGH);
        digitalWrite(LED3, HIGH);
        digitalWrite(BUZZER, HIGH);
        delay(500);
        digitalWrite(LED1, LOW);
        digitalWrite(LED2, LOW);
        digitalWrite(LED3, LOW);
        digitalWrite(BUZZER, LOW);
        delay(500);
      }
      jeu = true;
    }
  
  }
  if(Jeu_Select == "2"){
    if(currentMillis - previousMillis > intervalOn){
      previousMillis = currentMillis;
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      digitalWrite(LED3, LOW);
      Jeu_Select = "0";
      jeu = false;
    }
  }
  state = digitalRead(BTN1);
  state2 = digitalRead(BTN2);
  state3 = digitalRead(BTN3);
  
  /*if((state != prevState) && digitalRead(BTN1)){
      url = "/Stage-FFE/form.php?ip="+IPbis+"&btn=1";
      digitalWrite(LED1, LOW);
    }else{
      url = "/Stage-FFE/form.php?ip="+IPbis+"&btn=4";
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      digitalWrite(LED3, LOW);
    }
    datos = true;
  }
  if((state2 != prevState2) && digitalRead(BTN2)){
    
    client.stop();
    if(digitalRead(14) == HIGH){
      url = "/Stage-FFE/form.php?ip="+IPbis+"&btn=2";
      digitalWrite(LED2, LOW);
    }else{
      url = "/Stage-FFE/form.php?ip="+IPbis+"&btn=4";
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      digitalWrite(LED3, LOW);
    }
    datos = true;
  }
  if((state3 != prevState3) && digitalRead(BTN3)){
    client.stop();
    if(digitalRead(13) == HIGH){
      url = "/Stage-FFE/form.php?ip="+IPbis+"&btn=3";
      digitalWrite(LED3, LOW);
    }else{
      url = "/Stage-FFE/form.php?ip="+IPbis+"&btn=4";
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      digitalWrite(LED3, LOW);
    }
    datos = true;
  }*/
      if(datos){
        url += "&jeu=";
        url += Jeu_Select;
        //http.begin(host,httpPort,url);
        int httpCode = http.GET();
        http.end();
        datos = false;
      }
      if((digitalRead(12) == LOW) && (digitalRead(14) == LOW) && (digitalRead(13) == LOW)){
        jeu = false;
      }
      prevState = state;
      prevState2 = state2;
      prevState3 = state3;
}
void read_to_battery(){
    String client = client;
    String line = client.readStringUntil;*/
    WiFiClient client;
    client.println("HTTP/1.1 200 OK");
    client.println("Access-Control-Allow-Origin: *");
    client.println("Content-Type: text/html");
    client.println(""); 
    client.println("<!DOCTYPE HTML>");
    client.println("<html>");
    client.println("<br><br>");
    client.println("<a href=\"/?LED=100\"\"><button>PTS <b>5</b></button></a>");
    client.println("<a href=\"/?LED=010\"\"><button>PTS <b>2</b></button></a>");
    client.println("<a href=\"/?LED=001\"\"><button>PTS <b>1</b></button></a><br />");
    client.println("<a href=\"/?Start\"\"><button><b>JEU!</b></button></a><br />");
    //client.println("<a href=\"/?Niveau\"\"><button><b>Niveau</b></button></a><br />");
    client.println("id: "+id);
    client.println("IP: "+IP); 
    client.println("</html>");
    String line = client.readStringUntil('\r');
    client.flush();

  if(line.indexOf("/?Niveau") != -1){
    int usb = digitalRead(USB);
    if(usb==1){
      url+= "?USB=";
      url+= usb;
      url+= "&ip=";
      url+= IPbis;
      //http.begin(host,httpPort,url);
      int httpCode = http.GET();
      http.end();
      url="/Stage-FFE/form.php";
    }else{
    float Battery_level=0;
    float Battery_pourc=0;
    int bettery = analogRead(A0);
    Battery_level =  (float)bettery/1023; //mis en volts
    Battery_pourc = ((Battery_level-min_value)/(max_value-min_value))*100;  //mis en pourcentage
    if (Battery_pourc>87) {   //100
      Battery_pourc=100;
      url+= "?Niveau=";
      url+= Battery_pourc;
      url+= "&ip=";
      url+= IPbis;
      //http.begin(host,httpPort,url);
      int httpCode = http.GET();
      http.end();
      url="/Stage-FFE/form.php";
    } 
    if (Battery_pourc<=87 && Battery_pourc>62) {   //75
      Battery_pourc=75;
      url+= "?Niveau=";
      url+= Battery_pourc;
      url+= "&ip=";
      url+= IPbis;
      //http.begin(host,httpPort,url);
      int httpCode = http.GET();
      http.end();
      url="/Stage-FFE/form.php";
    }
    if (Battery_pourc<=62 && Battery_pourc>37) {   //50
      Battery_pourc=50;
      url+= "?Niveau=";
      url+= Battery_pourc;
      url+= "&ip=";
      url+= IPbis;
      //http.begin(host,httpPort,url);
      int httpCode = http.GET();
      http.end();
      url="/Stage-FFE/form.php";
    }
    if (Battery_pourc<=37) {   //25
      Battery_pourc=25;
      url+= "?Niveau=";
      url+= Battery_pourc;
      url+= "&ip=";
      url+= IPbis;
      //http.begin(host,httpPort,url);
      int httpCode = http.GET();
      http.end();
      url="/Stage-FFE/form.php";
    }
   }
  } 
}
}
  
